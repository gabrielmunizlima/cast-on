# Fiap CastOn

>  Documentação do trabalho - Projeto CastOn para o Trabalho de Conclusão da Disciplina ENGINEERING SOFTWARE DEVELOPMENT  - Prof.: ANDRE PONTES SAMPAIO



## Instalação - Pré-Requisitos:
* GitBash
* Docker Client / Docker Desktop com suporte  a Docker Compose

*OBS*: A aplicação poderá demorar cerca de 1 a 2 minutos para que todos os recursos fiquem prontos.
 
## Passo a Passo :

* Realize o clone do seguinte repositório:
```sh
git clone https://gitlab.com/gabrielmunizlima/Fiap-Netflix.git
```
## OPCAO 1 (compose com service-discovery e gateway)
* Acesse o diretório “Infra-Compose”
```sh
cd Infra-Compose
```
* Execute o comando para iniciar a stack:
```sh
docker-compose up --build
```
* Acesse a aplicação em http://localhost:8080/artista-service/swagger-ui/
 
## OPCAO 2 (apenas o microservico com banco de dados)
* Acesse o diretório “Infra-Compose” e "compose-simplificado"
```sh
cd Infra-Compose
cd compose-simplificado
```
* Execute o comando para iniciar a stack:
```sh
docker-compose up --build
```
* Acesse a aplicação em http://localhost:8091/swagger-ui/


## INSTRUÇÕES: Itens para pontuação da Nota - Itens realizados marcados com (x)

(x) UM MICROSERVIÇO OU CLASSE COM AÇÕES CRUD	3 PONTOS (+ 1 PONTO / MICROSERVIÇO).

( ) TESTES (UNITÁRIOS + INTEGRAÇÃO + UI)	    		+ 1  OU + 2 OU + 3 PONTOS

(x) USO DE BANCO DE DADOS 						+ 2 PONTOS

(x) DEPLOY VIA DOCKER								+ 1 PONTO

(x) COMUNICAÇÃO SINCRONA API	COM SWAGGER		+ 1 PONTOS

( ) FRONT END 										+ 2 PONTOS
