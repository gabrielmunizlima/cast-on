--popular tabelas

-- endereco
insert into endereco (cep,numero,referencia,rua,bairro,cidade,uf) values('00000-123',13,'quebrada','rua Ana','capão redondo','São Paulo','SP');

-- artista
insert into artista (nome, cpf, email, dtNasc,endereco_id) values('Mano Brown', '171.123.171.13','brown@gmail.com','1984-10-03',1);

-- agenda
insert into agenda(dtInicio,dtFim,artista_id) values('2020-10-30 19:00:00','2020-10-30 21:00:00',1);