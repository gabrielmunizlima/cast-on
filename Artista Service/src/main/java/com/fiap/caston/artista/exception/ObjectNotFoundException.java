package com.fiap.caston.artista.exception;

//TODO verificar se dá tempo de trocar por fallback
public class ObjectNotFoundException extends RuntimeException {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ObjectNotFoundException() {
         super();
     }
     public ObjectNotFoundException(String s) {
         super(s);
     }
     public ObjectNotFoundException(String s, Throwable throwable) {
         super(s, throwable);
     }
     public ObjectNotFoundException(Throwable throwable) {
         super(throwable);
     }
}
