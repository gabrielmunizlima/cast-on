
package com.fiap.caston.artista.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.fiap.caston.artista.model.Artista;
 

public interface ArtistaRepository extends JpaRepository<Artista, Long> {


}
