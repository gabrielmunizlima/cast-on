package com.fiap.caston.artista.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fiap.caston.artista.model.Artista;
import com.fiap.caston.artista.repository.ArtistaRepository;

@Service
public class ArtistaService {

	private final ArtistaRepository artistaRepo;

	@Autowired
	public ArtistaService(ArtistaRepository artistaRepo) {
		this.artistaRepo = artistaRepo;
	}

	public Artista consultarPorId(long id) {
		return artistaRepo.findById(id).get();
	}
	
	public Artista cadastrar(Artista artista) {
		return artistaRepo.save(artista);
	}
	
	public Artista atualizar(Artista artista) {
		
		if(artista == null)
			throw new IllegalArgumentException("Artista não foi informado!");  
		if(artista.getId() == null)
			throw new IllegalArgumentException("Id do Artista não foi informado!");
		
		//valida se o usuario existe
		  consultarPorId(artista.getId());
		 
		return artistaRepo.save(artista);
	}
	
	public void removerPorId(long id) {
		  artistaRepo.deleteById(id);
	}


}
