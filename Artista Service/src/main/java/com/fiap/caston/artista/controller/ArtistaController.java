package com.fiap.caston.artista.controller;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fiap.caston.artista.model.Artista;
import com.fiap.caston.artista.service.ArtistaService;

@RestController
@RequestMapping("/artistas")
public class ArtistaController {

	private final ArtistaService artistaService;

	private static final String ID_NOT_FOUND_MSG = "O artista informado não foi localizado.";

	@Autowired
	public ArtistaController(ArtistaService artistaService) {
		this.artistaService = artistaService;
	}

	@GetMapping(value = "{id}")
	public ResponseEntity<Artista> consultarArtistaPorId(@PathVariable long id) {
		try {
			return new ResponseEntity<>(artistaService.consultarPorId(id), HttpStatus.CREATED);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, ID_NOT_FOUND_MSG);
		}
	}

	@PostMapping()
	public ResponseEntity<Artista> cadastrarArtista(@RequestBody Artista artista) {
		return new ResponseEntity<>(artistaService.cadastrar(artista), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Artista> atualizarArtista(@RequestBody Artista artista) {
		try {
			return new ResponseEntity<>(artistaService.atualizar(artista), HttpStatus.OK);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "id do artista informado não localizado",
					e.getCause());
		} catch (IllegalArgumentException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}

	}

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Artista> deletarArtista(@PathVariable long id) {
		try {
			artistaService.removerPorId(id);
			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "id do artista informado não localizado",
					e.getCause());
		}
	}

}
