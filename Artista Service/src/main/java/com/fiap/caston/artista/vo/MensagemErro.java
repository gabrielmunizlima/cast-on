package com.fiap.caston.artista.vo;

public class MensagemErro {

	private String msg;

	public MensagemErro(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
