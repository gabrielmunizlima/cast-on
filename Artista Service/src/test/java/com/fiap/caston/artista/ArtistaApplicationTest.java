package com.fiap.caston.artista;

import com.fiap.caston.artista.controller.ArtistaController;
import com.fiap.caston.artista.model.Agenda;
import com.fiap.caston.artista.model.Artista;
import com.fiap.caston.artista.model.Endereco;
import com.fiap.caston.artista.repository.ArtistaRepository;
import com.fiap.caston.artista.service.ArtistaService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
@SpringBootTest
public class ArtistaApplicationTest {

    @Mock
    private ArtistaRepository artistaRepo;

    private ArtistaController artistaController;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ArtistaService artistaService = new ArtistaService(artistaRepo);
        artistaController = new ArtistaController(artistaService);
        Artista artista = montaUmArtista();
        artista.setId(1L);

        Mockito.when(artistaRepo.findById(1L)).thenReturn(java.util.Optional.of(artista));
    }

    @Test
    public void contextLoads() {
        //Testa Contexto
    }

    @Test
    public void createArtistaTest() {
        Artista artista = montaUmArtista();
        artista.setNome("Carlos Eduardo");
        artistaController.cadastrarArtista(artista);
        verify(artistaRepo, times(1)).save(artista);
    }

    @Test
    public void throwsArtistaNaoLocalizadoTest(){
        assertThrows(ResponseStatusException.class, () -> artistaController.consultarArtistaPorId(0));

        thrown.expectMessage("404 NOT_FOUND \"O artista informado não foi localizado.\"");
        artistaController.consultarArtistaPorId(0);
    }

    @Test
    public void updateArtistaTest() {
        Artista artista = montaUmArtista();
        artista.setId(1L);
        artista.setNome("Carlos Silva");

        Mockito.when(artistaRepo.save(artista)).thenReturn(artista);

        Artista novoArtista = artistaController.atualizarArtista(artista).getBody();

        assertEquals("1,Carlos Silva", novoArtista.getId() + "," + novoArtista.getNome());
    }

    @Test
    public void deleteArtistaTest() {
        artistaController.deletarArtista(1L);
        verify(artistaRepo, times(1)).deleteById(1L);
    }

    private static Date formatDate(String date) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").parse(date);
    }

    private static Date formatDateHour(String dateHour) throws ParseException {
        return new SimpleDateFormat("hh:mm dd/MM/yyyy").parse(dateHour);
    }

    private Artista montaUmArtista() {
        try {
            Artista artista = new Artista();
            artista.setNome("João Paulo");
            artista.setCpf("123.456.789-09");
            artista.setEmail("joao.paulo@email.com");
            artista.setDtNasc(formatDate("25/10/1980"));
            artista.setAgenda(montarAgenda());
            artista.setEndereco(montarEndereco());
            return artista;
        } catch (ParseException e){
            Artista artista = new Artista();
            artista.setNome("João Paulo");

            return artista;
        }
    }

    private List<Agenda> montarAgenda() throws ParseException {
        List<Agenda> agendas = new ArrayList<>();

        Agenda ag1 = new Agenda();
        ag1.setDtInicio(formatDateHour("18:15 01/12/2020"));
        ag1.setDtFim(formatDateHour("23:00 01/12/2020"));

        Agenda ag2 = new Agenda();
        ag2.setDtInicio(formatDateHour("15:00 07/12/2020"));
        ag2.setDtFim(formatDateHour("16:00 07/12/2020"));

        Agenda ag3 = new Agenda();
        ag3.setDtInicio(formatDateHour("22:00 14/01/2020"));
        ag3.setDtFim(formatDateHour("00:00 15/01/2020"));

        return agendas;
    }

    private Endereco montarEndereco() {
        Endereco endereco = new Endereco();
        endereco.setUf("SP");
        endereco.setCidade("São Paulo");
        endereco.setBairro("Centro");
        endereco.setCep("12345-678");
        endereco.setRua("Avenida Paulista");
        endereco.setNumero(54L);
        endereco.setReferencia("Casa 2");

        return endereco;
    }
}
